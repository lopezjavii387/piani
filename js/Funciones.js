function MFormInsertar(){
var parametros = {};

$.ajax({
	data: parametros,
	url: "FormInsertar.php",
	type: "POST",
	beforeSend: function() {
		$("#ventana").html("Procesando...");
	},
success: function(vista){
	$("#ventana").html(vista);
}


});

}

function  MDatosListar(){
var parametros = {};

$.ajax({
	data: parametros,
	url: "DatosListar.php",
	type: "POST",
	beforeSend: function(){
		$("#ventana").html("Procesando...");
	},
success: function(vista){
	$("#ventana").html(vista);
}


});

}


function InsertarRegistro(){
var parametros = {
	"vno": $("#Nombre").val(),
	"vma": $("#Matricula").val(),
	"vdi": $("#Direccion").val(),
	"vte": $("#Telefono").val(),
	"vco": $("#Correo_Electronico").val(),
};

$.ajax({
	data: parametros,
	url: "DatosInsertar.php",
	type: "POST",
	beforeSend: function(){
		$("#ventana").html("Procesando...");
	},
success: function(vista){
	$("#ventana").html(vista);
}


});

}

function Modificar(cod){
var parametros = {
	"vma": cod,
};

$.ajax({
	data: parametros,
	url: "FormModificar.php",
	type: "POST",
	beforeSend: function(){
		$("#ventana").html("Procesando...");
	},
success: function(vista){
	$("#ventana").html(vista);
}


});

}

function ModificarRegistro(){
var parametros = {

	"vno": $("#Nombre").val(),
	"vma": $("#Matricula").val(),
	"vdi": $("#Direccion").val(),
	"vte": $("#Telefono").val(),
	"vco": $("#Correo_Electronico").val(),
};

$.ajax({
	data: parametros,
	url: "DatosModificar.php",
	type: "POST",
	beforeSend: function(){
		$("#ventana").html("Procesando...");
	},
success: function(vista){
	$("#ventana").html(vista);
}


});

}



function Eliminar(cod){
var parametros = {
	"vid": cod,
};

$.ajax({
	data: parametros,
	url: "DatosEliminar.php",
	type: "POST",
	beforeSend: function(){
		$("#ventana").html("Procesando...");
	},
success: function(vista){
	$("#ventana").html(vista);
}


});

}